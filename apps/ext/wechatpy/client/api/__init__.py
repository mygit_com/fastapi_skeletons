# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from apps.ext.wechatpy.client.api.card import WeChatCard  # NOQA
from apps.ext.wechatpy.client.api.customservice import WeChatCustomService  # NOQA
from apps.ext.wechatpy.client.api.datacube import WeChatDataCube  # NOQA
from apps.ext.wechatpy.client.api.device import WeChatDevice  # NOQA
from apps.ext.wechatpy.client.api.group import WeChatGroup  # NOQA
from apps.ext.wechatpy.client.api.invoice import WeChatInvoice  # NOQA
from apps.ext.wechatpy.client.api.jsapi import WeChatJSAPI  # NOQA
from apps.ext.wechatpy.client.api.material import WeChatMaterial  # NOQA
from apps.ext.wechatpy.client.api.media import WeChatMedia  # NOQA
from apps.ext.wechatpy.client.api.menu import WeChatMenu  # NOQA
from apps.ext.wechatpy.client.api.merchant import WeChatMerchant  # NOQA
from apps.ext.wechatpy.client.api.message import WeChatMessage  # NOQA
from apps.ext.wechatpy.client.api.misc import WeChatMisc  # NOQA
from apps.ext.wechatpy.client.api.poi import WeChatPoi  # NOQA
from apps.ext.wechatpy.client.api.qrcode import WeChatQRCode  # NOQA
from apps.ext.wechatpy.client.api.scan import WeChatScan  # NOQA
from apps.ext.wechatpy.client.api.semantic import WeChatSemantic  # NOQA
from apps.ext.wechatpy.client.api.shakearound import WeChatShakeAround  # NOQA
from apps.ext.wechatpy.client.api.tag import WeChatTag  # NOQA
from apps.ext.wechatpy.client.api.template import WeChatTemplate  # NOQA
from apps.ext.wechatpy.client.api.user import WeChatUser  # NOQA
from apps.ext.wechatpy.client.api.wifi import WeChatWiFi  # NOQA
from apps.ext.wechatpy.client.api.wxa import WeChatWxa  # NOQA
from apps.ext.wechatpy.client.api.marketing import WeChatMarketing  # NOQA
