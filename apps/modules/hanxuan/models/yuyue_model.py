from peewee import *

# database = PostgresqlDatabase('zyxadminsystem', **{'host': 'localhost', 'port': 5432, 'user': 'postgres', 'password': '123456'})

from apps.ext.pooled_postgresql import sync_pooled_postgresql_client
from peewee import *
from playhouse.shortcuts import model_to_dict, dict_to_model

# 自定义的
import json


class JSONField(TextField):
    def db_value(self, value):
        return json.dumps(value)

    def python_value(self, value):
        if value is not None:
            return json.loads(value)


class UnknownField(object):
    def __init__(self, *_, **__): pass


class BaseModel(Model):
    class Meta:
        database = sync_pooled_postgresql_client.GetSession()


class Doctor(BaseModel):
    account = TextField(constraints=[SQL("DEFAULT ''::text")], index=True, null=True)
    addr = TextField(constraints=[SQL("DEFAULT ''::text")], null=True)
    ctime = DateTimeField(constraints=[SQL("DEFAULT now()")])
    describe = TextField(constraints=[SQL("DEFAULT ''::text")], null=True)
    destag = TextField(constraints=[SQL("DEFAULT ''::text")], null=True)
    dnmobile = TextField(constraints=[SQL("DEFAULT ''::text")], index=True, null=True)
    dnname = TextField(constraints=[SQL("DEFAULT ''::text")], index=True, null=True)
    dno = TextField(constraints=[SQL("DEFAULT ''::text")], index=True, null=True)
    enable = IntegerField(constraints=[SQL("DEFAULT 1")], null=True)
    fee = TextField(constraints=[SQL("DEFAULT ''::text")], null=True)
    grade = TextField(constraints=[SQL("DEFAULT ''::text")], null=True)
    pic = TextField(constraints=[SQL("DEFAULT ''::text")], index=True, null=True)
    rank = TextField(constraints=[SQL("DEFAULT ''::text")], null=True)
    room_no = TextField(constraints=[SQL("DEFAULT ''::text")], null=True)
    sex = IntegerField(constraints=[SQL("DEFAULT 3")], null=True)

    class Meta:
        table_name = 'doctor'


class DoctorNsnumInfo(BaseModel):
    ampm = TextField(constraints=[SQL("DEFAULT ''::text")], index=True, null=True)
    ctime = DateTimeField(constraints=[SQL("DEFAULT now()")], index=True)
    dno = TextField(constraints=[SQL("DEFAULT ''::text")], index=True, null=True)
    dnotime = DateField(null=True)

    tiempm = TextField(constraints=[SQL("DEFAULT ''::text")], index=True, null=True)
    # worktime = TextField(constraints=[SQL("DEFAULT ''::text")], null=True)
    nsindex = TextField(constraints=[SQL("DEFAULT ''::text")], index=True, null=True)
    nsnum = IntegerField(null=True)
    use_nsnum = IntegerField(null=True)

    class Meta:
        table_name = 'doctor_nsnum_info'


class DoctorNsnumOrder(BaseModel):
    ctime = DateTimeField(constraints=[SQL("DEFAULT now()")])
    dno = TextField(constraints=[SQL("DEFAULT ''::text")], index=True, null=True)
    orderid = TextField(constraints=[SQL("DEFAULT ''::text")], index=True, null=True)
    wx_orderid = TextField(constraints=[SQL("DEFAULT ''::text")], index=True, null=True)
    payactions = IntegerField(constraints=[SQL("DEFAULT 1")], null=True)
    payfee = TextField(constraints=[SQL("DEFAULT ''::text")], null=True)
    statue = IntegerField(index=True, null=True)
    visit_statue = IntegerField(constraints=[SQL("DEFAULT 1")], null=True)
    visit_sure_time = DateTimeField(null=True)
    visit_uage = IntegerField(null=True)
    visit_uname = TextField(constraints=[SQL("DEFAULT ''::text")], null=True)
    visit_uphone = TextField(constraints=[SQL("DEFAULT ''::text")], null=True)
    visit_usex = TextField(constraints=[SQL("DEFAULT ''::text")], null=True)
    visitday = TextField(constraints=[SQL("DEFAULT ''::text")], null=True)
    visittime = TextField(constraints=[SQL("DEFAULT ''::text")], null=True)
    visit_uopenid  = TextField(constraints=[SQL("DEFAULT ''::text")], null=True)

    nsindex = TextField(constraints=[SQL("DEFAULT ''::text")], null=True)
    wx_gzx_id = TextField(constraints=[SQL("DEFAULT ''::text")], null=True)
    wx_mch_id = TextField(constraints=[SQL("DEFAULT ''::text")], null=True)
    prepay_id = TextField(constraints=[SQL("DEFAULT ''::text")], null=True)
    nonce_str = TextField(constraints=[SQL("DEFAULT ''::text")], null=True)
    mweb_url = TextField(constraints=[SQL("DEFAULT ''::text")], null=True)
    pay_sign = TextField(constraints=[SQL("DEFAULT ''::text")], null=True)
    trade_type = TextField(constraints=[SQL("DEFAULT ''::text")], null=True)
    is_subscribe = TextField(constraints=[SQL("DEFAULT ''::text")], null=True)
    notify_callback_time = DateTimeField(null=True)
    doctot_name  = TextField(constraints=[SQL("DEFAULT ''::text")], null=True)

    class Meta:
        table_name = 'doctor_nsnum_order'


class Hospital(BaseModel):
    describe = TextField(constraints=[SQL("DEFAULT ''::text")], null=True)
    describeimages = TextField(constraints=[SQL("DEFAULT ''::text")], null=True)
    name = TextField(constraints=[SQL("DEFAULT ''::text")], index=True, null=True)

    class Meta:
        table_name = 'hospital'


class VisitUserInfo(BaseModel):
    ctime = DateTimeField(constraints=[SQL("DEFAULT now()")])
    openid = TextField(constraints=[SQL("DEFAULT ''::text")], index=True, null=True)
    statue = IntegerField(null=True)
    uage = IntegerField(null=True)
    unicename = TextField(constraints=[SQL("DEFAULT ''::text")], index=True, null=True)
    uphone = TextField(constraints=[SQL("DEFAULT ''::text")], null=True)
    usex = TextField(constraints=[SQL("DEFAULT ''::text")], null=True)

    class Meta:
        table_name = 'visit_user_info'
