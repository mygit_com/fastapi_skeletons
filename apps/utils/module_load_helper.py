#!/usr/bin/evn python
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   文件名称 :     module_load_helper
   文件功能描述 :   功能描述
   创建人 :       小钟同学
   创建时间 :          2021/6/18
-------------------------------------------------
   修改描述-2021/6/18:         
-------------------------------------------------
"""
from importlib import import_module
from pathlib import Path


def asterisk(path: Path, package, globals):
    """
    Imports all symbols from all modules in the `path`
    """
    for module_path in path.glob("*.py"):
        if module_path.is_file() and not module_path.stem.startswith("_"):
            module = import_module(f".{module_path.stem}", package=package)
            symbols = [
                symbol for symbol in module.__dict__ if not symbol.startswith("_")
            ]
            globals.update({symbol: getattr(module, symbol) for symbol in symbols})
