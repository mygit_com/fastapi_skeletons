#!/usr/bin/evn python
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   文件名称 :     wxpay_conf
   文件功能描述 :   功能描述
   创建人 :       小钟同学
   创建时间 :          2021/9/27
-------------------------------------------------
   修改描述-2021/9/27:         
-------------------------------------------------
"""
from functools import lru_cache
from pydantic import BaseSettings
import pprint
from pydantic import AnyUrl, BaseSettings
import os
pp = pprint.PrettyPrinter(indent=4)


class WxPaySettings(BaseSettings):
    pass
    #公众号-开发者ID(AppID)
    GZX_ID: str = '' # 微信公众号ID
    #公众号-开发者密码
    GZX_SECRET:str = ''
    GZX_PAY_KEY: str = '' # 微信支付秘钥
    MCH_ID: str = '' # 微信支付ID
    NOTIFY_URL =   'http://127.0.0.1:9080/hs/api/v1/doctor/subscribe/paycallback' #支付回调






@lru_cache()
def get_settings():
    return WxPaySettings()

# 配置实例的对象的创建
wxpayconf = get_settings()
